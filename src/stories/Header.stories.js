import React from "react"

import { Header } from "./Header"

export default {
  title: "Example/Header",
  component: Header,
}

const Template = (args) => <Header {...args} />
export const Default = Template.bind({})
Default.args = {
  user: {},
  onLogin: () => {},
  onLogout: () => {},
  onCreateAccount: () => {},
}

export const LoggedIn = Template.bind({})
LoggedIn.args = {
  ...Default.args,
}

export const LoggedOut = Template.bind({})
LoggedOut.args = {
  ...Default.args,
}
